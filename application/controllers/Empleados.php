<?php
class Empleados extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //cargar modelo
    $this->load->model('Empleado');
  }
  public function index(){
    $data['empleados']=$this->Empleado->obtenerTodos();
    $this->load->view('header');
    $this->load->view('empleados/index',$data);
    $this->load->view('footer');
  }
  //funcion que renderiza vista Nuevo
  public function nuevo()
  {
    $this->load->view('header');
    $this->load->view('empleados/nuevo');
    $this->load->view('footer');
  }
  public function guardar(){
    $datosNuevoEmpleado=array(
      "apellido_emp"=>$this->input->post('apellido_emp'),
      "nombre_emp"=>$this->input->post('nombre_emp'),
      "cargo_emp"=>$this->input->post('cargo_emp'),
      "genero_emp"=>$this->input->post('genero_emp'),
      "estado_civil_emp"=>$this->input->post('estado_civil_emp'),
      "fecha_nacimiento_emp"=>$this->input->post('fecha_nacimiento_emp')
    );
  if ($this->Empleado->insertar($datosNuevoEmpleado)) {
    redirect('empleados/index');
  }else{
    echo"<h1>ERROR AL INSERTAR</h1>";
   }
  }
  //funcion para eliminar Clientes
  public function eliminar($id_emp){
    if ($this->Empleado->borrar($id_emp)) {
      redirect('empleados/index');
    }else{
      echo "ERROR AL BORRAR :(";
    }
  }
}//cierre de la clase





 ?>
