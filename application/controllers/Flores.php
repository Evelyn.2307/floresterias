<?php
class Flores extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //cargar modelo
    $this->load->model('Flor');
  }
  public function index(){
    $data['flores']=$this->Flor->obtenerTodos();
    $this->load->view('header');
    $this->load->view('flores/index',$data);
    $this->load->view('footer');
  }
  //funcion que renderiza vista Nuevo
  public function nuevo()
  {
    $this->load->view('header');
    $this->load->view('flores/nuevo');
    $this->load->view('footer');
  }
  public function guardar(){
    $datosNuevoFlor=array(
      "nombre_flo"=>$this->input->post('nombre_flo'),
      "color_flo"=>$this->input->post('color_flo'),
      "estacion_flo"=>$this->input->post('estacion_flo'),
      "fecha_siembra_flo"=>$this->input->post('fecha_siembra_flo'),
      "precio_flo"=>$this->input->post('precio_flo')
    );
  if ($this->Flor->insertar($datosNuevoFlor)) {
    redirect('flores/index');
  }else{
    echo"<h1>ERROR AL INSERTAR</h1>";
   }
  }
  //funcion para eliminar Clientes
  public function eliminar($id_flo){
    if ($this->Flor->borrar($id_flo)) {
      redirect('flores/index');
    }else{
      echo "ERROR AL BORRAR :(";
    }
  }
}//cierre de la clase





 ?>
