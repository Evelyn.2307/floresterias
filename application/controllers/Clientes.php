<?php
class Clientes extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //cargar modelo
    $this->load->model('Cliente');
  }
  public function index(){
    $data['clientes']=$this->Cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('clientes/index',$data);
    $this->load->view('footer');
  }
  //funcion que renderiza vista Nuevo
  public function nuevo()
  {
    $this->load->view('header');
    $this->load->view('clientes/nuevo');
    $this->load->view('footer');
  }
  public function guardar(){
    $datosNuevoCliente=array(
      "nombre_cli"=>$this->input->post('nombre_cli'),
      "direccion_cli"=>$this->input->post('direccion_cli'),
      "telefono_cli"=>$this->input->post('telefono_cli'),
      "cedula_cli"=>$this->input->post('cedula_cli'),
      "email_cli"=>$this->input->post('email_cli')
    );
  if ($this->Cliente->insertar($datosNuevoCliente)) {
    redirect('clientes/index');
  }else{
    echo"<h1>ERROR AL INSERTAR</h1>";
   }
  }
  //funcion para eliminar Clientes
  public function eliminar($id_cli){
    if ($this->Cliente->borrar($id_cli)) {
      redirect('clientes/index');
    }else{
      echo "ERROR AL BORRAR :(";
    }
  }
}//cierre de la clase





 ?>
