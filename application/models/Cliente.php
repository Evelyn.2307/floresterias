<?php
class Cliente extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  //insertar instructor en la base de datos mysql
  function insertar($datos){
    return $this->db->insert("clientes",$datos);
  }
  public function obtenerTodos(){
    $listadoClientes=$this->db->get("clientes");
    if ($listadoClientes->num_rows()>0){
      return $listadoClientes->result();
    }else{
      return false;
    }
  }
  //borrar cliente
  function borrar($id_cli){
    $this->db->where("id_cli",$id_cli);
    if($this->db->delete("clientes")){
      return true;
    }else{
      return false;
    }
  }
}//cierre de la clase


 ?>
