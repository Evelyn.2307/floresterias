<?php
class Flor extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  //insertar instructor en la base de datos mysql
  function insertar($datos){
    return $this->db->insert("flores",$datos);
  }
  public function obtenerTodos(){
    $listadoFlores=$this->db->get("flores");
    if ($listadoFlores->num_rows()>0){
      return $listadoFlores->result();
    }else{
      return false;
    }
  }
  //borrar cliente
  function borrar($id_flo){
    $this->db->where("id_flo",$id_flo);
    if($this->db->delete("flores")){
      return true;
    }else{
      return false;
    }
  }
}//cierre de la clase


 ?>
