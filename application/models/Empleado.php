<?php
class Empleado extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  //insertar instructor en la base de datos mysql
  function insertar($datos){
    return $this->db->insert("empleados",$datos);
  }
  public function obtenerTodos(){
    $listadoEmpleados=$this->db->get("empleados");
    if ($listadoEmpleados->num_rows()>0){
      return $listadoEmpleados->result();
    }else{
      return false;
    }
  }
  //borrar cliente
  function borrar($id_emp){
    $this->db->where("id_emp",$id_emp);
    if($this->db->delete("empleados")){
      return true;
    }else{
      return false;
    }
  }
}//cierre de la clase


 ?>
