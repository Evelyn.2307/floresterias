<div class="row">
  <div class="col-md-8">
    <center><h1 class="text-center glyphicon glyphicon-user"><b><font color=" #d5ace6">LISTADO DE EMPLEADOS</b></font></h1></center>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url('empleados/nuevo'); ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>
      Agregar nuevo empleado
    </a>
  </div>
</div>
<br>
<?php if($empleados): ?>
  <table class="table table-striped
    table-bordered table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>APELLIDO</th>
             <th>NOMBRE</th>
             <th>CARGO</th>
             <th>GENERO</th>
             <th>ESTADO CIVIL</th>
             <th>FECHA DE NACIMIENTO</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($empleados as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_emp; ?>
              </td>
              <td>
                <?php echo $filaTemporal->apellido_emp; ?>
              </td>
              <td>
                <?php echo $filaTemporal->nombre_emp; ?>
              </td>
              <td>
                <?php echo $filaTemporal->cargo_emp; ?>
              </td>
              <td>
                <?php echo $filaTemporal->genero_emp; ?>
              </td>
              <td>
                <?php echo $filaTemporal->estado_civil_emp; ?>
              </td>
              <td>
                <?php echo $filaTemporal->fecha_nacimiento_emp; ?>
              </td>
              <td class="text-center">
              <a href="#" title="Editar Empleado" style="color:black;">
                <button type="submit" name="button"
                class="btn btn-primary">
                  Editar
                  &nbsp;
                </button>
                </a>
              &nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/empleados/eliminar/<?php echo $filaTemporal->id_emp; ?>"
                 title="Eliminar empleado" onclick="return confirm('¿estas seguro de eliminar el registro seleccionado?');" style="color:black;">
                 <button type="submit" name="button"
                 class="btn btn-danger">
                 Eliminar
               </button>
                 </a>
              </td>
            </tr>

          <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1><font color="red">No hay Empleados Registradas</font></h1>
<?php endif; ?>
