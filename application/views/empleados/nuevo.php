<center><h1  class="glyphicon glyphicon-pencil"><font color="#f5801e">NUEVO EMPLEADO</h1></center>
  <br>
<form class=""
action="<?php echo site_url(); ?>/empleados/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Apellidos:</label>
          <br>
          <input type="text"
          placeholder="Porfavor ingrese sus apellidos"
          class="form-control"
          name="apellido_emp" value=""
          id="apellido_emp">
      </div>
      <div class="col-md-4">
          <label for="">Nombres:</label>
          <br>
          <input type="text"
          placeholder="Ingrese sus nombres"
          class="form-control"
          name="nombre_emp" value=""
          id="nombre_emp">
      </div>
      <div class="col-md-4">
        <label for="">Cargo:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el cargo que desempeña"
        class="form-control"
        name="cargo_emp" value=""
        id="cargo_emp">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Género:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su genero"
          class="form-control"
          name="genero_emp" value=""
          id="genero_emp">
      </div>
      <div class="col-md-4">
          <label for="">Estado Civil:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su estado civil"
          class="form-control"
          name="estado_civil_emp" value=""
          id="estado_civil_emp">
      </div>
      <div class="col-md-4">
          <label for="">Fecha de Nacimiento:</label>
          <br>
          <input type="date"
          placeholder="Seleccione su fecha de nacimiento"
          class="form-control"
          name="fecha_nacimiento_emp" value=""
          id="fecha_nacimiento_emp">
      </div> <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br>
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/empleados/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
