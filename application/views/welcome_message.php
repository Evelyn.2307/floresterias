<center><img src="<?php echo base_url(); ?>/assets/images/imagen1.png" alt="logo" width="150px" height="100px">
<center><h1><b><font color="#ef1bef">"FLORESTERIA DANAE"</b></h1></font>
<h1><b>“Solo estás aquí para una breve visita. No te apures, no te preocupes.
	 Y asegúrate de oler las flores a lo largo del camino.</b></h1></center>
	<br>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url(); ?>assets/images/girasol.jpg" alt="Germany" width="1030px" height="460px">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>assets/images/loto.jpg" alt="French" width="1030px" height="460px">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>assets/images/rosada.jpeg" alt="English" width="1030px" height="460px">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>
<center>
<div class="row"></div>
     <div class="col-md-6"><h2 class="text-center"><font color=" #ef1bef"><b>MISIÓN</b></h3> </font>
       <br>
       <h3 class="text-justify">Ser una floristería próspera y comprometida,
				  que ofrezca a sus clientes una gran variedad de productos florales y decoración que cumplan con las más altas exigencias
					de calidad de acuerdo a cada ocasión a través de una atención personalizada y profesional, el ofrecimiento alternativas que cumplan
					con los requerimientos solicitados por el cliente, considerándole siempre la prioridad.
          Tomamos muy en cuenta el reciclaje y la correcta utilización de los recursos naturales para contribuir
          con la conservación del medio ambiente. .</h3>
     </div>

     <div class="col-md-6"><h2 class="text-center"><font color=" #ef1bef"><b>VISIÓN</b></h2> </font>
       <br>
       <h3 class="text-justify">Innovar continuamente las técnicas para la elaboración
				 de los productos y los servicios ofrecidos, brindando siempre una atención de alta calidad al cliente.
				 Convertirse en una empresa sólida, competente para capacitar a otras personas en diferentes áreas
				  del campo de la floristería con el objetivo de lograr la contratación de personal con un salario justo.
					Ser reconocida por una amplia experiencia y una gran gama de productos naturales y artificiales
					 para lograr una exquisita decoración desde los pequeños detalles hasta los grandes eventos.</h3>
     </div>
       </div>
		 </center>
<br>
<br>
