<div class="row">
  <div class="col-md-8">
    <center><h1 class="text-center glyphicon glyphicon-asterisk"><b><font color="#d5ace6">LISTADO DE FLORES</b></font></h1>
  </div>
  <div class="col-md-4">
    <center> <a href="<?php echo site_url('flores/nuevo'); ?>"class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>
      Agregar Flores
    </a>
  </div>

</div>
<br>
<?php if($flores): ?>
  <table class="table table-striped
    table-bordered table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>NOMBRE</th>
             <th>COLOR</th>
             <th>ESTACION</th>
             <th>FECHA DE SIEMBRA</th>
             <th>PRECIO</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($flores as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_flo; ?>
              </td>
              <td>
                <?php echo $filaTemporal->nombre_flo; ?>
              </td>
              <td>
                <?php echo $filaTemporal->color_flo; ?>
              </td>
              <td>
                <?php echo $filaTemporal->estacion_flo; ?>
              </td>
              <td>
                <?php echo $filaTemporal->fecha_siembra_flo; ?>
              </td>
              <td>
                <?php echo $filaTemporal->precio_flo; ?>
              </td>
              <td class="text-center">
              <a href="#" title="Editar Flor" style="color:black;">
              <!-- <i class="glyphicon glyphicon-pencil"></i> -->
              <button type="submit" name="button"
              class="btn btn-primary">
                Editar
                &nbsp;
              </button>
              </a>
              &nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/flores/eliminar/<?php echo $filaTemporal->id_flo; ?>"
                 title="Eliminar Flor" onclick="return confirm('¿estas seguro de eliminar el registro seleccionado?');"style="color:black;">
              <!-- <i class="glyphicon glyphicon-trash"></i> -->
              <button type="submit" name="button"
              class="btn btn-danger">
                Eliminar
              </button>
              </a>
              </td>
            </tr>

          <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1><font color="red">No hay Flores Registradas</font></h1>
<?php endif; ?>
