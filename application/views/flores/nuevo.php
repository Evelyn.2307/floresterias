<center><font color=" #503396"><h1 class="glyphicon glyphicon-grain">NUEVA FLOR</h1></center>
<form class=""
action="<?php echo site_url(); ?>/flores/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Porfavor ingrese el nombre de la flor"
          class="form-control"
          name="nombre_flo" value=""
          id="nombre_flo">
      </div>
      <div class="col-md-4">
          <label for="">Color:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el color de la flor"
          class="form-control"
          name="color_flo" value=""
          id="color_flo">
      </div>
      <div class="col-md-4">
        <label for="">Estacion:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la estacion en la que se encuentra"
        class="form-control"
        name="estacion_flo" value=""
        id="estacion_flo">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Fecha de Siembra:</label>
          <br>
          <input type="date"
          placeholder="Seleccione la fecha de siembra"
          class="form-control"
          name="fecha_siembra_flo" value=""
          id="fecha_siembra_flo">
      </div>
      <div class="col-md-8">
          <label for="">Precio:</label>
          <br>
          <input type="number"
          placeholder="ingrese el precio de la flor"
          class="form-control"
          name="precio_flo" value=""
          id="precio_flo">
      </div> <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br>
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/flores/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
