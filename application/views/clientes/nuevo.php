<center><h1 class="glyphicon glyphicon-plus"><font color=" #089a0c">NUEVOCLIENTE</h1></center>
  <br>
<form class=""
action="<?php echo site_url(); ?>/clientes/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Porfavor ingrese su nombre completo"
          class="form-control"
          name="nombre_cli" value=""
          id="nombre_cli">
      </div>
      <div class="col-md-4">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su direccion"
          class="form-control"
          name="direccion_cli" value=""
          id="direccion_cli">
      </div>
      <div class="col-md-4">
        <label for="">Teléfono:</label>
        <br>
        <input type="number"
        placeholder="Ingrese su numero de celular"
        class="form-control"
        name="telefono_cli" value=""
        id="telefono_cli">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese los 10 digitos de su cedula"
          class="form-control"
          name="cedula_cli" value=""
          id="cedula_cli">
      </div>
      <div class="col-md-8">
          <label for="">Email:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su correo electronico"
          class="form-control"
          name="email_cli" value=""
          id="email_cli">
      </div> <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br>
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
