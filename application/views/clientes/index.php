<div class="row">
  <div class="col-md-8">
    <h1>LISTADO DE CLIENTES</h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url('clientes/nuevo'); ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>
     Agregar Clientes
    </a>
  </div>
</div>
<?php if($clientes): ?>
  <table class="table table-striped
    table-bordered table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>NOMBRE</th>
             <th>DIRECCIÓN</th>
             <th>TELÉFONO</th>
             <th>CEDULA</th>
             <th>EMAIL</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
          <?php foreach ($clientes as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_cli; ?>
              </td>
              <td>
                <?php echo $filaTemporal->nombre_cli; ?>
              </td>
              <td>
                <?php echo $filaTemporal->direccion_cli; ?>
              </td>
              <td>
                <?php echo $filaTemporal->telefono_cli; ?>
              </td>
              <td>
                <?php echo $filaTemporal->cedula_cli; ?>
              </td>
              <td>
                <?php echo $filaTemporal->email_cli; ?>
              </td>
              <td class="text-center">
              <a href="#" title="Editar Cliente" style="color:orange;">
                <button type="submit" name="button"
                class="btn btn-primary">
                  Editar
                  &nbsp;
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli; ?>"
                 title="Eliminar cliente" onclick="return confirm('¿Esta seguro de querer eliminar el registro seleccionado?');"style="color:red;">
                 <button type="submit" name="button"
                 class="btn btn-danger">
                 Eliminar
               </button>
              </a>
              </td>
            </tr>

          <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1><font color="red">No hay Clientes Registradas</font></h1>
<?php endif; ?>
